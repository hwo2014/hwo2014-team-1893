using System;
using System.Collections.Generic;
using System.Linq;

public class Point
{
	public float x { get; set; }
	public float y { get; set; }
}

public class OrientedPosition
{
	public Point position { get; set; }
	public float angle { get; set; }

}

public class InPieceStrategy{
	public int lane { get; set; }
	public double start {get; set;}
	public double end { get; set;}
	public List<SendMsg> instructions { get; set; }
	public int lastUpdatedGameTick { get; set; }
	public bool isSlipAngleDangerous { get; set; }
	public bool isSlipAngleVerified { get; set; }

}

public class TrackPiece
{
	public TrackPiece()
	{
		@switch = false;
	}

	public float length { get; set; }
	public bool @switch { get; set; }
	public float radius { get; set; }
	public float angle { get; set; }

	public bool IsStraight{
		get{ 
			if (radius == 0)
				return true; 
			return false; 
		}
	}

	public double Length (Lane lane)
	{
		if (this.IsStraight) {
			return length;
		} else {
			return (radius + lane.distanceFromCenter) * 2 * Math.PI * (Math.Abs (angle) / 360);
		}
	}

	public List<InPieceStrategy> plan { get; set; }

	public string PrintPiece(bool withPlan = false)
	{
		string detail = "Type: " + (this.IsStraight ? "Straight" :
		(this.angle < 0 ? "Turn Left, angle: " + this.angle : 
				"Turn Right, angle: " + this.angle));
		if(withPlan)
			foreach (var p in plan) {
				detail += "\n\tLane: "+ p.lane +" Pos: " + p.start + "," + p.end;
				var inst = p.instructions.FirstOrDefault ();
				if (inst != null)
					detail += " Inst: " + inst.ToJson ();
			}
		return detail;
	}
}

public class Lane
{
	public int distanceFromCenter { get; set; }
	public int index { get; set; }
	public double length { get; set; }
}

public class Track
{
	public string id { get; set; }
	public string name { get; set; }
	public List<TrackPiece> pieces { get; set; }
	List<Lane> _lanes;
	public List<Lane> lanes { get { return _lanes; } set{ _lanes = value; calculateLaneLength (); } }
	public OrientedPosition startingPoint { get; set; }
	public bool IsTurboAvailable { get; set; }
	public CarPosition currentPosition { get; set;}

	void calculateLaneLength()
	{
		foreach (var piece in this.pieces) {
			foreach (var lane in this._lanes) {
				if (piece.IsStraight)
					lane.length += piece.length;
				else
					lane.length += (piece.radius + lane.distanceFromCenter) * 2 * Math.PI * (Math.Abs (piece.angle) / 360);
			}
		}
	}

	public InPiecePosition TryGetAdvancedPiecePosition(InPiecePosition startPosition, int advanceLength)
	{
		InPiecePosition advancedPosition = new InPiecePosition () {
			pieceIndex = startPosition.pieceIndex,
			inPieceDistance = startPosition.inPieceDistance + advanceLength
		};

		int innerBreakCount = 0;
		while (true && ++innerBreakCount < 25) {
			float pieceLength = (float) lanes.Max (l => pieces [advancedPosition.pieceIndex].Length (l));
			if (advancedPosition.inPieceDistance <= pieceLength) {
				break;
			} else {
				advancedPosition.inPieceDistance -= pieceLength;
				advancedPosition.pieceIndex = NextPiece (advancedPosition.pieceIndex);
			}
		}

		return innerBreakCount >= 25 ? null : advancedPosition;
	}


	public InPiecePosition TryGetPriorPiecePosition(InPiecePosition startPosition, int priorLength)
	{
		InPiecePosition priorPosition = new InPiecePosition () {
			pieceIndex = startPosition.pieceIndex,
			inPieceDistance = startPosition.inPieceDistance - priorLength
		};

		int innerBreakCount = 0;
		while (true && ++innerBreakCount < 25) {

			if (priorPosition.inPieceDistance >= 0) {
				break;
			} else {
				priorPosition.pieceIndex = PreviousPiece (priorPosition.pieceIndex);
				float pieceLength = (float) lanes.Max (l => pieces [priorPosition.pieceIndex].Length (l));
				priorPosition.inPieceDistance += pieceLength;
			}
		}

		return innerBreakCount >= 25 ? null : priorPosition;
	}


	public int NextPiece(int currentPiece) {
		return currentPiece + 1 < this.pieces.Count ? currentPiece + 1 : 0;
	}

	public int PreviousPiece(int currentPiece) {
		return currentPiece - 1 > 0 ? currentPiece - 1 : this.pieces.Count() -1;
	}

	public string PrintPlan()
	{
		var detail = "";
		foreach (var piece in pieces) {
			detail +=piece.PrintPiece(true)+"\n";
		}
		return detail;
	}

	public InPieceStrategy GetPieceStrategy(CarPosition info)
	{
		InPieceStrategy st = this.pieces[info.piecePosition.pieceIndex].plan.FirstOrDefault(
			s => s.lane == info.piecePosition.lane.startLaneIndex &&
			s.start >= info.piecePosition.inPieceDistance
		);
		if (st == null)
			st = this.pieces [info.piecePosition.pieceIndex].plan.Last (
				s => s.lane == info.piecePosition.lane.startLaneIndex);
		return st;
	}
}

public class CarDimension
{
	public float length { get; set; }
	public float width { get; set; }
	public float guideFlagPosition { get; set; }
}


public class Car
{
	public CarData id { get; set; }
	public CarDimension dimensions { get; set; }

	public override bool Equals (object obj)
	{
		Car carObj = obj as Car;
		return carObj != null && id.Equals (carObj.id);
	}

	public override int GetHashCode ()
	{
		return id.GetHashCode ();
	}
}

public class RaceSession
{
	public int laps { get; set; }
	public int maxLapTimeMs { get; set; }
	public bool quickRace { get; set; }
}

public class RaceData
{
	public Track track { get; set; }
	public List<Car> cars { get; set; }
	public RaceSession raceSession { get; set; }

	public bool writeToLog { get; set; }
}

public class GameInitData
{
	public RaceData race { get; set; }
}

class TurboCarInfo{
	public string name {get; set;}
	public string color {get; set;}
}
