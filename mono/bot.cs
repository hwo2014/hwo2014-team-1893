using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;

public class Bot
{
	static string trackName = "";
	static bool createRace = true;
	static int carCount = 1;
	static double multiplier = 1.0d;

	public static void Main (string[] args)
	{
		string host = args [0];
		int port = int.Parse (args [1]);
		string botName = args [2];
		string botKey = args [3];
		trackName = (args.Length >= 5)? args[4] : "";
		createRace = (args.Length >= 6) ? bool.Parse (args [5]) : true;
		carCount = (args.Length >= 7) ? int.Parse (args [6]) : 1;
		multiplier = (args.Length >= 8) ? double.Parse (args [7]) : 1.0;

		Console.WriteLine ("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using (TcpClient client = new TcpClient (host, port)) {
			NetworkStream stream = client.GetStream ();
			StreamReader reader = new StreamReader (stream);
			StreamWriter writer = new StreamWriter (stream);
			writer.AutoFlush = true;

			new Bot (reader, writer, new Join (botName, botKey));
		}
	}

	private StreamWriter writer;
	private SpeedTelemetry speedTelemetry;
	private TelemetryPlanAdjuster planAdjuster;
	CarData myCar = null;
	public static List<IObserver<CarPositionsMsg>> carPositionsObservers = new List<IObserver<CarPositionsMsg>> ();
	public static List<IObserver<CrashMsg>> crashObservers = new List<IObserver<CrashMsg>> ();
	public static List<IObserver<LapFinishedMsg>> lapObservers = new List<IObserver<LapFinishedMsg>> ();


	Bot (StreamReader reader, StreamWriter writer, Join join)
	{
		this.writer = writer;

		string line;
		CarPositionHandler provider = new CarPositionHandler ();
		CarPositionMonitor observer1 = null;
		RaceData trackInfo = null;
		double currentThrottle = 0.0;

		// create a new race in germany
		if (!string.IsNullOrEmpty (trackName)) {
			CreateRaceData raceData = new CreateRaceData () {
				botId = new CarBot () {
					key = join.key,
					name = join.name
				},
				carCount = carCount,
				trackName = trackName
			};
			if(createRace)
				send (new CreateRace ( raceData));
			else
				send (new JoinRace (raceData));
		}
		else
			send (join);

		while ((line = reader.ReadLine ()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper> (line);
			try {
				switch (msg.msgType) {

				case "carPositions":
					var carPositions = JsonConvert.DeserializeObject<CarPositionsMsg> (line);
					var car = carPositions.positions.First (c => c.id.Equals(myCar));
					car.currentThrottle = currentThrottle;
					provider.CarStatus (car);
					currentThrottle = car.currentThrottle;
					LogToConsole(car, carPositions, trackInfo);
					carPositionsObservers.ForEach(o => o.OnNext(carPositions));
					if (carPositions.gameTick % 300 == 0)
						planAdjuster.AdjustTrack(trackInfo.track);
					break;
				case "join":
					Console.WriteLine ("Joined");
					send (new Ping ());
					break;
				case "gameInit":
					Console.WriteLine (line);
					Console.WriteLine ("Race init");
				//Get the track info
					if (trackInfo == null) {
						trackInfo = (JsonConvert.DeserializeObject<GameInitMsg> (line)).raceData;
						trackInfo.track = new DummyStrategy (trackInfo.track, multiplier).Calculate ();
						trackInfo.writeToLog = true;
					}
				// instantiate the observer with the track info
					if (observer1 == null) {
						observer1 = new CarPositionMonitor (trackInfo, writer);
						provider.Subscribe (observer1);
						var crashMonitor = new CrashMonitor(trackInfo, myCar);
						crashObservers.Add(crashMonitor);
						carPositionsObservers.Add(crashMonitor);
					}
					InitializeSpeedTelemetry (trackInfo);
					InitializeAngleObservers(trackInfo);
				//send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine ("Race ended");
					send (new Ping ());
					PrintDifferentialTelemetry (myCar);
					Console.WriteLine ("Max Speeds: " +
						string.Join ("; ", speedTelemetry.GetMaxObservedSpeedsSummary ().Select (d => d.ToString ("n2"))));
					planAdjuster.AdjustTrack(trackInfo.track);
					break;
				case "gameStart":
					Console.WriteLine ("Race starts");
					currentThrottle = 0;
					send (new Throttle (1));
					break;
				case "yourCar":
					var carMsg = JsonConvert.DeserializeObject<YourCarMsg> (line);
					myCar = new CarData () { name = carMsg.name, color = carMsg.color };
					send (new Ping ());
					break;
				case "lapFinished":
					var lapMsg = JsonConvert.DeserializeObject<LapFinishedMsg> (line);
					lapObservers.ForEach (o => o.OnNext (lapMsg));
					send (new Ping ());

					if (lapMsg.LapFinishedData.car.color == myCar.color 
						&& lapMsg.LapFinishedData.car.name == myCar.name)
					{
						PrintDifferentialTelemetry (myCar);
						Console.WriteLine ("Max Speeds: " +
							string.Join ("; ", speedTelemetry.GetMaxObservedSpeedsSummary ().Select (d => d.ToString ("n2"))));

						var ranking = lapMsg.LapFinishedData.ranking;
						planAdjuster.SetEnabledStatus( ranking.fastestLap != 1 && ranking.overrall != 1);
					}

					break;
				case "crash":
					var crashMsg = JsonConvert.DeserializeObject<CrashMsg> (line);
					crashObservers.ForEach (o => o.OnNext (crashMsg));
					if (crashMsg.carId.Equals(myCar))
					{
						planAdjuster.SetEnabledStatus(false);
						trackInfo.writeToLog = false;
					}
					send (new Ping ());
					break;
				case "turboAvailable":
					trackInfo.track.IsTurboAvailable = true;
					break;
				case "turboEnd":
					var turboEnd = JsonConvert.DeserializeObject<TurboEndMsg> (line);
					if (turboEnd.color == myCar.color )
						trackInfo.track.IsTurboAvailable = false;
					break;
				case "spawn":
					Console.WriteLine(line);
					var spawnMsg = JsonConvert.DeserializeObject<SpawnMsg>(line);
					if (spawnMsg.carId.Equals(myCar))
					{
						planAdjuster.SetEnabledStatus(true);
						trackInfo.writeToLog = true;
					}
					break;
				default:
					Console.WriteLine (line);
				//send(new Ping());
					break;
				}
			} catch (Exception e) {
				Console.WriteLine ("** ERROR ** : " + e.Message + "\n" + e.StackTrace);
			}
		}
	}

	void LogToConsole(CarPosition car, CarPositionsMsg carPositions, RaceData trackInfo)
	{
		if(trackInfo.writeToLog && carPositions.gameTick % 20 == 0)
			Console.WriteLine("Piece: " + car.piecePosition.pieceIndex + ", Pos:" + 
				car.piecePosition.inPieceDistance + car.currentThrottle+", tick: " + 
				carPositions.gameTick+ ", Piece Detail: " + 
				(trackInfo.track.pieces[car.piecePosition.pieceIndex].IsStraight?"Straight":
					trackInfo.track.pieces[car.piecePosition.pieceIndex].angle <0? "Turn Left, angle: "+
					trackInfo.track.pieces[car.piecePosition.pieceIndex].angle :"Turn Right, angle: "+ 
					trackInfo.track.pieces[car.piecePosition.pieceIndex].angle)+", CrrLane: "+
				car.piecePosition.lane.startLaneIndex+ ", SlipAngle: "+ car.angle);
	}


	void InitializeAngleObservers (RaceData trackInfo)
	{
		if (carPositionsObservers.Count (o => o is DangerousAngleObserver) == 0) {
			carPositionsObservers.Add (new DangerousAngleObserver (trackInfo.track, myCar));
		}
	}


	void InitializeSpeedTelemetry (RaceData trackInfo)
	{
		if (speedTelemetry == null) {
			speedTelemetry = new SpeedTelemetry (trackInfo);
			carPositionsObservers.Add (speedTelemetry);
			crashObservers.Add (speedTelemetry);
			lapObservers.Add (speedTelemetry);
			planAdjuster = new TelemetryPlanAdjuster (myCar, speedTelemetry);
		}
		speedTelemetry.StartGame ();
	}



	void PrintDifferentialTelemetry (CarData targetCar)
	{
		List<double> speedDiffs = speedTelemetry.DifferentialTelemetrySummary (targetCar);
		Console.WriteLine ("Speed Telemetry against competitors: " +
		string.Join ("; ", speedDiffs.Select (d => d.ToString ("n2"))));
	}

	private void send (SendMsg msg)
	{
		writer.WriteLine (msg.ToJson ());
	}
}



