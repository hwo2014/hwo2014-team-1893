﻿using System;
using System.Linq;
using System.Collections.Generic;

public class DangerousAngleObserver : IObserver<CarPositionsMsg>
{
	private Track track;
	private CarData carId;
	private double adjustDistance;

	public DangerousAngleObserver (Track track, CarData carId)
	{
		this.track = track;
		this.carId = carId;
		adjustDistance = 0.05 * (int) track.lanes.Average (l => l.length);
	}

	#region IObserver implementation
	public void OnCompleted ()
	{ }

	public void OnError (Exception error)
	{ }

	public void OnNext (CarPositionsMsg value)
	{
		var myPosition = value.positions.First (c => c.id.Equals(carId));
		if (Math.Abs(myPosition.angle) > 48) {
			MarkPrecedingPlansDangerous (myPosition);
		}
	}

	private void MarkPrecedingPlansDangerous (CarPosition myPosition)
	{
		int currentPieceIndex = myPosition.piecePosition.pieceIndex;
		foreach (var plan in track.pieces[currentPieceIndex].plan) {
			if (plan.start <= myPosition.piecePosition.inPieceDistance) {
				plan.isSlipAngleDangerous = true;
			}
		}

		int markedDistance = (int) myPosition.piecePosition.inPieceDistance;
		while (markedDistance < adjustDistance)
		{
			currentPieceIndex = track.PreviousPiece (currentPieceIndex);
			foreach (var plan in track.pieces[currentPieceIndex].plan) {
				plan.isSlipAngleDangerous = true;
			}
			markedDistance += (int) track.lanes.Max (l => track.pieces [currentPieceIndex].Length (l));
		}

	}
	#endregion
}
	

