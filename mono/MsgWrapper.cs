using System;
using System.Collections.Generic;

public class MsgWrapper {
	public string msgType;
	public Object data;

	public MsgWrapper(string msgType, Object data) {
		this.msgType = msgType;
		this.data = data;
	}
}

class YourCarMsg: MsgWrapper {
	public string name;
	public string color;

	public YourCarMsg (string msgType, CarData data)
		: base (msgType, data)
	{
		this.name = data.name;
		this.color = data.color;
		Console.WriteLine(
			String.Format("Your Car's Name: {0}\tYour car's color: {1}", this.name, this.color));
	}
}

public class LapFinishedMsg : MsgWrapper
{
	public LapFinishedData LapFinishedData;

	public LapFinishedMsg (string msgType, LapFinishedData data)
		: base (msgType, data)
	{
		this.LapFinishedData = data;
	}
}

public class CarPositionsMsg: MsgWrapper {
	public List<CarPosition> positions;
	public int gameTick { get; set; }
	public string gameId { get; set; }

	public CarPositionsMsg (string msgType, List<CarPosition> data)
		: base (msgType, data)
	{
		this.positions = data;
	}
}

class TurboEndMsg : MsgWrapper
{
	public string color;
	public TurboEndMsg(string msgType, TurboCarInfo data)
		: base (msgType, data)
	{
		color = data.color;
	}
}

public class GameInitMsg: MsgWrapper {
	public RaceData raceData { get; set; }
	public string color { get; set; }

	public GameInitMsg (string msgType, GameInitData data)
		: base (msgType, data)
	{
		this.raceData = data.race;
		Console.WriteLine ("track: "+ raceData.track.name);
		Console.WriteLine ("# cars in race: " + raceData.cars.Count);
		Console.WriteLine ("# track pieces: " + raceData.track.pieces.Count);
		Console.WriteLine ("# laps: " + raceData.raceSession.laps);
	}
}

public class CrashMsg : MsgWrapper
{
	public CarData carId;

	public CrashMsg (string msgType, CarData data)
		: base (msgType, data)
	{
		this.carId = data;
	}
}

public class SpawnMsg : MsgWrapper
{
	public CarData carId;

	public SpawnMsg (string msgType, CarData data)
		: base (msgType, data)
	{
		this.carId = data;
	}
}