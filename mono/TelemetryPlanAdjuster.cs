using System;
using System.Linq;

public class TelemetryPlanAdjuster
{
	private CarData carId;
	private SpeedTelemetry telemetry;
	private bool isEnabled = true;


	public TelemetryPlanAdjuster (CarData carId, SpeedTelemetry telemetry)
	{
		this.carId = carId;
		this.telemetry = telemetry;
	}

	public void SetEnabledStatus(bool status)
	{
		isEnabled = status;
	}


	public void AdjustTrack (Track track)
	{
		if (!isEnabled)
			return;

		var differentialSummary = telemetry.DifferentialTelemetrySummary (carId);
		var maxDifferential = differentialSummary.Max (s => Math.Abs (s));
		var meanDifferential = differentialSummary.Average (s => Math.Abs (s));

		for (int pieceIndex = 0; pieceIndex < track.pieces.Count; ++pieceIndex) {
			foreach (var plan in track.pieces[pieceIndex].plan.Where(p => !p.isSlipAngleDangerous && p.isSlipAngleVerified)) {
				//to see whether to adjust this plan, we want to look at the speed differential 
				//for the section 3 segments ahead
				InPiecePosition thisPosition = new InPiecePosition () { 
					pieceIndex = pieceIndex, 
					inPieceDistance = (float)plan.start 
				};
				InPiecePosition futurePosition = track.TryGetAdvancedPiecePosition (thisPosition, 30);
				if (futurePosition == null)
					continue;
				InPiecePosition futureEndPosition = track.TryGetAdvancedPiecePosition (futurePosition, 10);
				if (futureEndPosition == null)
					continue;

				var speedDiff = telemetry.DifferentialTelemetry (carId, futurePosition, futureEndPosition).FirstOrDefault ();
				if (IsAdjustmentNeeded (speedDiff, maxDifferential, meanDifferential)) {
					foreach (var msg in plan.instructions.Where(s => s is Throttle)) {
						var throttleMsg = msg as Throttle;
						if (throttleMsg != null) {
							double oldThrottle = throttleMsg.value;
							throttleMsg.value = Math.Min(1.0 - oldThrottle, oldThrottle) * 0.15 + oldThrottle; //todo: should factor in the differential somehow
						}
					}
				}
			}
		}

	}

	private bool IsAdjustmentNeeded (double speedDiff, double maxDifferential, double meanDifferential)
	{
		return speedDiff < -1 
			&& Math.Abs (speedDiff) > 0.5 * maxDifferential 
			&& meanDifferential > 1;
	}
}


