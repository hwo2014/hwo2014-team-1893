using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;

public class CarPositionsMsgProvider
{
	List<CarPositionsMsg> messages = new List<CarPositionsMsg>();


	public CarPositionsMsgProvider(String filename)
	{
		using (StreamReader reader = new StreamReader (filename)) {
			String line;
			while ((line = reader.ReadLine ()) != null) {
				messages.Add (JsonConvert.DeserializeObject<CarPositionsMsg> (line));
			}
		}
	}

	public IEnumerable<CarPositionsMsg> GetMessages()
	{
		return messages.AsEnumerable ();
	}

}

public class MultiPositionsMsgProvider
{
	List<MsgWrapper> messages = new List<MsgWrapper>();

	public MultiPositionsMsgProvider(String filename)
	{
		using (var reader = new StreamReader (filename)) {
			String line;
			while ((line = reader.ReadLine ()) != null) {
				var genericMsg = JsonConvert.DeserializeObject<MsgWrapper> (line);

				switch (genericMsg.msgType) {
				case "carPositions": 
					messages.Add (JsonConvert.DeserializeObject<CarPositionsMsg> (line));
					break;
				case "crash":
					messages.Add (JsonConvert.DeserializeObject<CrashMsg> (line));
					break;
				case "lapFinished":
					messages.Add (JsonConvert.DeserializeObject<LapFinishedMsg> (line));
					break;
				default:
					throw new ArgumentException ("not expecting type: " + genericMsg.msgType);
				}
			}

		}
	}

	public IEnumerable<MsgWrapper> GetMessages()
	{
		return messages.AsEnumerable ();
	}
}
