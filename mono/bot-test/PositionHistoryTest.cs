using NUnit.Framework;
using System;
using System.Linq;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Collections.Generic;

namespace bottest
{
	[TestFixture ()]
	public class PositionHistoryTest
	{
		public static String keimolaInitString = 
			@"			{""msgType"":""gameInit"",""data"":{""race"":{""track"":{""id"":""keimola"",""name"":""Keimola"",""pieces"":[{""length"":100.0} ,{""length"":100.0} ,{""length"":100.0} ,{""length"":100.0,""switch"":true} ,{""radius"":100,""angle"":45.0} ,{""radius"":100,""angle"":45.0} ,{""radius"":100,""angle"":45.0} ,{""radius"":100,""angle"":45.0} ,{""radius"":200,""angle"":22.5,""switch"":true} ,{""length"":100.0} ,{""length"":100.0} ,{""radius"":200,""angle"":-22.5} ,{""length"":100.0} ,{""length"":100.0,""switch"":true} ,{""radius"":100,""angle"":-45.0} ,{""radius"":100,""angle"":-45.0} ,{""radius"":100,""angle"":-45.0} ,{""radius"":100,""angle"":-45.0} ,{""length"":100.0,""switch"":true} ,{""radius"":100,""angle"":45.0} ,{""radius"":100,""angle"":45.0} ,{""radius"":100,""angle"":45.0} ,{""radius"":100,""angle"":45.0} ,{""radius"":200,""angle"":22.5} ,{""radius"":200,""angle"":-22.5} ,{""length"":100.0,""switch"":true} ,{""radius"":100,""angle"":45.0} ,{""radius"":100,""angle"":45.0} ,{""length"":62.0} ,{""radius"":100,""angle"":-45.0,""switch"":true} ,{""radius"":100,""angle"":-45.0} ,{""radius"":100,""angle"":45.0} ,{""radius"":100,""angle"":45.0} ,{""radius"":100,""angle"":45.0} ,{""radius"":100,""angle"":45.0} ,{""length"":100.0,""switch"":true} ,{""length"":100.0} ,{""length"":100.0} ,{""length"":100.0} ,{""length"":90.0} ],""lanes"":[{""distanceFromCenter"":-10,""index"":0} ,{""distanceFromCenter"":10,""index"":1} ],""startingPoint"":{""position"":{""x"":-300.0,""y"":-44.0} ,""angle"":90.0}} ,""cars"":[{""id"":{""name"":""jen"",""color"":""purple""} ,""dimensions"":{""length"":40.0,""width"":20.0,""guideFlagPosition"":10.0}} ],""raceSession"":{""laps"":3,""maxLapTimeMs"":60000,""quickRace"":true}}} ,""gameId"":""9ee86239-4c19-4bc5-a768-f03e3e519a8c""}";

		public static String simpleInitString = 
			@"			{""msgType"":""gameInit"",""data"":{""race"":{""track"":{""id"":""keimola"",""name"":""Keimola"",""pieces"":[{""length"":100.0} ,{""length"":100.0} ,{""length"":100.0}],""lanes"":[{""distanceFromCenter"":-10,""index"":0} ,{""distanceFromCenter"":10,""index"":1} ],""startingPoint"":{""position"":{""x"":-300.0,""y"":-44.0} ,""angle"":90.0}} ,""cars"":[{""id"":{""name"":""jen"",""color"":""purple""} ,""dimensions"":{""length"":40.0,""width"":20.0,""guideFlagPosition"":10.0}} ],""raceSession"":{""laps"":3,""maxLapTimeMs"":60000,""quickRace"":true}}} ,""gameId"":""9ee86239-4c19-4bc5-a768-f03e3e519a8c""}";

		private static PositionHistory _simpleLoopObservedLap = null;

		private PositionHistory GetTrackInfo(String gameInitSerialization)
		{
			var trackInfo = (JsonConvert.DeserializeObject<GameInitMsg> (gameInitSerialization)).raceData;
			Dictionary<int, int> pieceOffsets = new Dictionary<int, int> ();

			pieceOffsets.Add (0, 0); 
			for (int i = 1; i < trackInfo.track.pieces.Count; ++i) {
				var previousPiece = trackInfo.track.pieces [i - 1];
				if (previousPiece.IsStraight) {
					pieceOffsets.Add (i, 
						pieceOffsets [i - 1] + (int)previousPiece.length);
				} else {
					pieceOffsets.Add (i,
						pieceOffsets [i - 1] + (int)trackInfo.track.lanes.Max (l => previousPiece.Length (l)));
				}
			}
			int trackLength = pieceOffsets [trackInfo.track.pieces.Count - 1]
			                  + (int)trackInfo.track.lanes.Max (l => trackInfo.track.pieces.Last ().Length (l));

			int capacity = (int)trackInfo.track.lanes.Max (l => l.length) / 5 + 1;
			List<SpeedData> speedData = new List<SpeedData> (capacity);
			for (int i = 0; i < capacity; ++i) {
				speedData.Add (new SpeedData (i*5));
			}

			return new PositionHistory (speedData, pieceOffsets, trackLength);
		}

		private PositionHistory SimpleLoopObservedLap()
		{
			if (_simpleLoopObservedLap == null) {
				_simpleLoopObservedLap = GetTrackInfo (simpleInitString);
				var positionProvider = new CarPositionsMsgProvider ("SimpleLoop.txt");
				var lap1Messages = positionProvider.GetMessages ().Take (9);
				var lap2Messages = positionProvider.GetMessages ().Skip (9);
				foreach (var msg in lap1Messages) {
					_simpleLoopObservedLap.OnNext (msg.positions.First());
				}
				_simpleLoopObservedLap.PromotePendingSpeeds (300); //track length
				foreach (var msg in lap2Messages) {
					_simpleLoopObservedLap.OnNext (msg.positions.First());
				}
			}

			return _simpleLoopObservedLap;
		}


		[Test ()]
		public void PositionHistory_GivenNoMessages_WhenToString_ThenReturnsAllZeros ()
		{

			var target = GetTrackInfo (keimolaInitString);
			//double trackLength = 3600;

			String expected = String.Empty;
			for (int i = 0; i < 722; ++i) {
				expected += "; 0.00";
			}
			expected = expected.Substring (2);

			Assert.AreEqual (expected, target.ToString ());
		}

		[Test ()]
		public void PositionHistory_GivenStartingMessagesFor219Distance_WhenToString_ThenSpeedsNotSetBeyond172Distance ()
		{
			var target = GetTrackInfo (keimolaInitString);

			var positionProvider = new CarPositionsMsgProvider("KeimolaFullRacePositions.txt");
			foreach (var msg in positionProvider.GetMessages().Take(75)) { //first 75 messages get you to distance 219
				target.OnNext (msg.positions.First());
			}

			var actual = target.ToString ().Split (';');
			int lastSetDistanceIndex = (219 - target.PendingLength) / 5;
			for(int i = 0; i< actual.Length; ++i){
				if (0 < i && i <= lastSetDistanceIndex )
					Assert.AreNotEqual (0, Double.Parse (actual [i]), "Failed at index=" + i);
				else
					Assert.AreEqual (0, Double.Parse (actual [i]),  "Failed at index=" + i);
			}

		}


		[Test ()]
		public void PositionHistory_GivenSimpleTrackLoopAround_WhenLoopsAround_ThenReturnsBeginningSpeed ()
		{
			var target = SimpleLoopObservedLap ();
			Console.WriteLine (target.ToString ());
			String expected = "112.00; 112.00; 112.00; 112.00; 112.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 20.00; 48.00; 48.00; 48.00; 48.00; 48.00; 48.00; 48.00; 48.00; 48.00; 55.00; 55.00; 55.00; 55.00; 55.00; 55.00; 55.00; 55.00; 55.00; 55.00; 55.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00; 112.00";
			Assert.AreEqual (expected, target.ToString ());
		}


		[Test ()]
		public void PositionHistory_GivenSimpleTrackLoopAround_WhenGetDistanceAt13_ThenReturns112 ()
		{
			var target = SimpleLoopObservedLap ();

			double actual = target.GetMaxObservedSpeed (new InPiecePosition () { pieceIndex = 0, inPieceDistance  = 13 });
			double expected = 112;
			Assert.AreEqual (expected, actual);
		}


		[Test ()]
		public void PositionHistoryPerformance_GivenIncomingMessages_WhenQuerying_ThenAnswersIn100ms ()
		{
			var target = GetTrackInfo (keimolaInitString);
			var positionProvider = new CarPositionsMsgProvider ("KeimolaFullRacePositions.txt");
			int messageCount = 0;

			var stopwatch = Stopwatch.StartNew ();
			foreach (var msg in positionProvider.GetMessages()) {
				target.OnNext (msg.positions.First());
				var myPos = msg.positions.First ().piecePosition;
				target.GetMaxObservedSpeed (new InPiecePosition () 
					{ pieceIndex = myPos.pieceIndex, inPieceDistance  = myPos.inPieceDistance + 0.5f });
				++messageCount;
			}
			stopwatch.Stop ();

			Assert.LessOrEqual (stopwatch.ElapsedMilliseconds / (double) messageCount, 1);
		}





	}
}

