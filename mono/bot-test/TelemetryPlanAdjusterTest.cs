using NUnit.Framework;
using System;
using Newtonsoft.Json;
using System.Diagnostics;

namespace bottest
{
	[TestFixture ()]
	public class TelemetryPlanAdjusterTest
	{
		[Test ()]
		public void DontCrashPlease ()
		{
			Track track = JsonConvert.DeserializeObject<GameInitMsg> (PositionHistoryTest.keimolaInitString).raceData.track;

			var telemetry = SpeedTelemetryTest.GetTelemetryForRace ();
			var planAdjuster = new TelemetryPlanAdjuster (SpeedTelemetryTest.cars [1].id, telemetry);

			var dummyStrategy = new DummyStrategy (track, 1);
			track = dummyStrategy.Calculate ();

			var stopwatch = Stopwatch.StartNew ();
			planAdjuster.AdjustTrack (track);
			stopwatch.Stop ();
			Assert.Less (stopwatch.Elapsed, TimeSpan.FromMilliseconds (500));
			Assert.Ignore (stopwatch.ElapsedMilliseconds.ToString());

		}


		[Test ()]
		public void TryGetPriorPiece ()
		{
			Track track = JsonConvert.DeserializeObject<GameInitMsg> (PositionHistoryTest.keimolaInitString).raceData.track;
			var dummyStrategy = new DummyStrategy (track, 1);
			track = dummyStrategy.Calculate ();

			InPiecePosition actual = track.TryGetPriorPiecePosition (new InPiecePosition () { inPieceDistance = 10, pieceIndex = 27 }, 297);
			Assert.AreEqual(23, actual.pieceIndex);

		}
	}
}

