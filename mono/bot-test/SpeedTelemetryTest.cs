using NUnit.Framework;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace bottest
{
	[TestFixture ()]
	public class SpeedTelemetryTest
	{

		public static List<Car> cars = null;
		private static MultiPositionsMsgProvider messageProvider = null;

		public static SpeedTelemetry GetTelemetryForRace(int messageCount = -1)
		{
			var raceData = JsonConvert.DeserializeObject<GameInitMsg> (PositionHistoryTest.keimolaInitString).raceData;
			raceData.cars.Clear ();
			raceData.cars.Add (new Car () {
				id = new CarData () { name = "jenSlow", color = "blue" },
				dimensions = new CarDimension () { length = 40, width = 20, guideFlagPosition = 10 }				
			});
			raceData.cars.Add (new Car () {
				id = new CarData () { name = "jenFastSlow", color = "yellow" },
				dimensions = new CarDimension () { length = 40, width = 20, guideFlagPosition = 10 }				
			});
			raceData.cars.Add (new Car () {
				id = new CarData () { name = "jenFast", color = "red" },
				dimensions = new CarDimension () { length = 40, width = 20, guideFlagPosition = 10 }				
			});
			cars = raceData.cars;

			var speedTelemetry = new SpeedTelemetry (raceData);
			messageProvider = new MultiPositionsMsgProvider ("multiLog.txt");
			IEnumerable<MsgWrapper> messages = messageCount == -1 ?
			                                   messageProvider.GetMessages ()
			                                   : messageProvider.GetMessages ().Take (messageCount);

			foreach (var msg in messages) {
				if (msg is CarPositionsMsg) {
					((IObserver<CarPositionsMsg>)speedTelemetry).OnNext ((CarPositionsMsg) msg);
				} else if (msg is CrashMsg) {
					((IObserver<CrashMsg>)speedTelemetry).OnNext ((CrashMsg) msg);
				} else if (msg is LapFinishedMsg) {
					((IObserver<LapFinishedMsg>)speedTelemetry).OnNext ((LapFinishedMsg) msg);
				}
			}

			return speedTelemetry;
		}


		[Test ()]
		public void From3CarRace ()
		{
			var speedTelemetry = GetTelemetryForRace ();
			Console.WriteLine (speedTelemetry.GetFormattedTelemetry ());
			foreach (Car car in cars) {
				List<double> speedDiffs = speedTelemetry.DifferentialTelemetrySummary (car.id);
				Console.WriteLine(car.id.color + " against competitors: " + 
					string.Join ("; ", speedDiffs.Select(d => d.ToString ("n2"))));
			}
		}


		[Test ()]
		public void FakeQualifyingAndRace ()
		{
			var speedTelemetry = GetTelemetryForRace (1835);
			Console.WriteLine (speedTelemetry.GetFormattedTelemetry ());
			foreach (Car car in cars) {
				List<double> speedDiffs = speedTelemetry.DifferentialTelemetrySummary (car.id);
				Console.WriteLine(car.id.color + " against competitors: " + 
					string.Join ("; ", speedDiffs.Select(d => d.ToString ("n2"))));
			}

			speedTelemetry.StartGame ();
			foreach (var msg in messageProvider.GetMessages()) {
				if (msg is CarPositionsMsg) {
					((IObserver<CarPositionsMsg>)speedTelemetry).OnNext ((CarPositionsMsg) msg);
				} else if (msg is CrashMsg) {
					((IObserver<CrashMsg>)speedTelemetry).OnNext ((CrashMsg) msg);
				} else if (msg is LapFinishedMsg) {
					((IObserver<LapFinishedMsg>)speedTelemetry).OnNext ((LapFinishedMsg) msg);
				}
			}

			Console.WriteLine (speedTelemetry.GetFormattedTelemetry ());
			foreach (Car car in cars) {
				List<double> speedDiffs = speedTelemetry.DifferentialTelemetrySummary (car.id);
				Console.WriteLine(car.id.color + " against competitors: " + 
					string.Join ("; ", speedDiffs.Select(d => d.ToString ("n2"))));
			}
		}


		[Test()]
		public void SpeedTelemetry_GivenPositionsOnTwoTrackPieces_WhenDifferentialTelemetry_ThenSpansThePieces()
		{
			var speedTelemetry = GetTelemetryForRace ();

			var actual = speedTelemetry.DifferentialTelemetry(cars[1].id, 
				new InPiecePosition() { pieceIndex =  2, inPieceDistance = 75},
				new InPiecePosition() { pieceIndex = 3, inPieceDistance = 15});

			String expected = "-3.10; -3.08; -3.06; -3.04";
			String actualString =  (String.Join ("; ", actual.Select (d => d.ToString ("n2"))));
			Assert.AreEqual (expected, actualString);
		}

	}
}

