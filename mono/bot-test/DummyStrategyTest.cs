using NUnit.Framework;
using System;
using System.Linq;
using Newtonsoft.Json;

namespace bottest
{
	[TestFixture ()]
	public class DummyStrategyTest
	{
		[Test ()]
		public void ExploratoryTest ()
		{
			var raceData = JsonConvert.DeserializeObject<GameInitMsg> (PositionHistoryTest.keimolaInitString).raceData;
			var target = new DummyStrategy (raceData.track, 1);

			var track = target.Calculate ();
			var plan1 = track.pieces [0].plan;
			var plan4 = track.pieces [3].plan;

			var curvePlan = track.pieces.First (p => !p.IsStraight).plan;

			Assert.Ignore ("blah");
		}
	}
}

