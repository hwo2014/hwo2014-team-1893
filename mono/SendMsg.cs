using System;
using Newtonsoft.Json;

public abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
		return this;
	}

	protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "purple";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class Turbo : SendMsg{
	public string value { get; set; }
	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "turbo";
	}
}

class CreateRace: SendMsg {
	public CreateRaceData data;
	public CreateRace (CreateRaceData raceData)
	{
		this.data = raceData;
	}
	protected override Object MsgData() {
		return this.data;
	}
	protected override string MsgType() { 
		return "createRace";
	}
}

class JoinRace : SendMsg{
	public CreateRaceData data;
	public JoinRace(CreateRaceData raceData)
	{
		this.data = raceData;
	}
	protected override Object MsgData() {
		return this.data;
	}
	protected override string MsgType(){
		return "joinRace";
	}
}

class SwitchLane : SendMsg
{
	public string value { get; set; }
	protected override string MsgType() {
		return "switchLane";
	}
	public SwitchLane (string lane)
	{
		this.value = lane;
	}
	protected override Object MsgData() {
		return this.value;
	}
}