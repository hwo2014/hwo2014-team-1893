using System;
using System.Collections.Generic;


public class LaneIndex
{
	public int startLaneIndex { get; set;}
	public int endLaneIndex { get; set;}
}

public class InPiecePosition
{
	public int pieceIndex { get; set;}
	public float inPieceDistance { get; set;}
	public LaneIndex lane {get; set;}
	public int lap { get; set; }

	public bool IsBefore(InPiecePosition compareTo)
	{
		return pieceIndex < compareTo.pieceIndex
		|| (pieceIndex == compareTo.pieceIndex && inPieceDistance < compareTo.inPieceDistance);
	}
}


public class CarPosition
{
	public CarData id { get; set; }
	public float angle { get; set; }
	public InPiecePosition piecePosition { get; set; }

	public double currentThrottle { get; set; }
	public bool isTurboAvailable { get; set; }
} 


