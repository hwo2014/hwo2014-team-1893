using System;

public class LapTimeData
{
	public int lap;
	public int ticks;
	public int millis;
}

public class RaceTimeData
{
	public int laps;
	public int ticks;
	public int millis;
}

public class RankingData
{
	public int overrall;
	public int fastestLap;
}

public class LapFinishedData
{
	public CarData car;
	public LapTimeData lapTime;
	public RaceTimeData raceTime;
	public RankingData ranking;
}




