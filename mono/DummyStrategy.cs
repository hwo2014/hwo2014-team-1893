﻿using System;
using System.Collections.Generic;
using System.Linq;

public class DummyStrategy
{
	Track track;
	double throttle = 0;
	double maxThrottle = 1;

	public DummyStrategy (Track t, double multiplier)
	{
		track = t;
		float factor = CalculateTurnFactor (t);
		throttle = CalculateConstantThrotleForTrack () * factor * multiplier;
		maxThrottle = Math.Min (1.0, factor * multiplier);
	}
	private double CalculateConstantThrotleForTrack (){
		double totalLength = 0;
		double straightsLength = 0;
		double turnsLength = 0;
		foreach (var piece in track.pieces) {
			if (piece.IsStraight) {
				totalLength += piece.length;
				straightsLength += piece.length;
			}
			else {
				double l = piece.Length (track.lanes.First ());
				totalLength += l;
				turnsLength += l;
			}
		}
		double straightPerc = straightsLength / totalLength;
		double turnPec = turnsLength / totalLength;
		if (straightPerc > turnPec)
			return straightPerc;
		return turnPec;
	}



	public Track Calculate()
	{
		for(int j = 0; j < track.pieces.Count; j++)
		{
			var piece = track.pieces [j];
			if (piece.plan == null)
				piece.plan = new List<InPieceStrategy> ();
			foreach (var lane in track.lanes) {
				double length = piece.Length (lane);
				for (double i = 0; i < length; i = i + 10) {
					InPieceStrategy segmentStrategy = new InPieceStrategy () {
						lane = lane.index,
						start = i,
						instructions = new List<SendMsg> ()
					};
					segmentStrategy.instructions.Add (CalculateMessage(piece, j, i, length));
					segmentStrategy.isSlipAngleVerified = piece.IsStraight;
					piece.plan.Add (segmentStrategy);

				}
			}
		}
		return this.track;
	}

	SendMsg CalculateMessage(TrackPiece piece, int trackPieceIndex, double position, double pieceLength)
	{
		string switchDirection = ShouldSwitch( trackPieceIndex, position, pieceLength);
		if (!string.IsNullOrEmpty (switchDirection))
			return new SwitchLane (switchDirection);
		else {
			if (shouldAccelerate (trackPieceIndex))
				return new Throttle (maxThrottle);
			else if (isNextTurnHairpin (trackPieceIndex))
				return new Throttle (throttle * .8);
			return new Throttle (throttle);
		}
	}

	string ShouldSwitch(int currentPiece, double distance, double pieceLength)
	{
		int nextPiece = track.NextPiece(currentPiece);
		TrackPiece nextBend = track.pieces.Skip (nextPiece).FirstOrDefault (c => !c.IsStraight);

		if (nextBend == null)
			return "";

		float angle = nextBend.angle;

		if (track.pieces [nextPiece].@switch && distance > pieceLength * .6) {
			if (angle < 0)
				return "Left";
			return "Right";
		}
		return "";
	}
	bool isNextTurnHairpin(int currentPiece)
	{
		int nextPiece = track.NextPiece(currentPiece);
		bool isBend = false;
		float totalAngle = 0;
		for (int i = nextPiece; i < track.pieces.Count; i++) {
			if (!track.pieces [i].IsStraight) {
				isBend = true;
				totalAngle += track.pieces [i].angle;
			} else if (isBend)
				break;
		}
		if (totalAngle >= 180)
			return true;
		return false;
	}

	bool shouldAccelerate(int currentPiece){

		int nextPiece = track.NextPiece(currentPiece);
		int twoPiecesAhead = track.NextPiece(nextPiece);
		int threePiecesAhead = track.NextPiece(twoPiecesAhead);

		bool isBend = false;
		float totalAngle = 0;
		for (int i = nextPiece; i < track.pieces.Count; i++) {
			if (!track.pieces [i].IsStraight) {
				isBend = true;
				totalAngle += track.pieces [i].angle;
			} else if (isBend)
				break;
		}
		if ((Math.Abs(totalAngle) <= 90 && track.pieces [currentPiece].IsStraight &&
			track.pieces [nextPiece].IsStraight) || 
			(Math.Abs(totalAngle) >= 180 && track.pieces [currentPiece].IsStraight &&
				track.pieces [nextPiece].IsStraight &&
				track.pieces [twoPiecesAhead].IsStraight &&
				track.pieces [threePiecesAhead].IsStraight ) )
			return true;
		return false;
	}


	float CalculateTurnFactor(Track track)
	{
		//keimola = 2.75
		// germany = 3.875
		// france = 3
		// usa = 1
		float factor = track.pieces.Where (c => !c.IsStraight).Sum (c => Math.Abs (c.angle)) / 360;
		if (factor < 2)
			return  1;
		else if (factor < 2.7)
			return .95f;
		else if (factor < 3.5)
			return .85f;
		else if (factor < 4)
			return .8f;
		else if (factor <= 6)
			return .75f;
		return 1;
	}
}


