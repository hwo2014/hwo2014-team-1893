using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;


public enum VarianceLevel
{
	None = 0,
	Low = 1,
	Medium = 2,
	High = 3,
	SuperHigh = 4
}


public class AngleHistory
{
	private List<Tuple<int, float, InPiecePosition>> angles = new List<Tuple<int, float, InPiecePosition>>();
	private List<Tuple<double, VarianceLevel>> varianceTolerances = new List<Tuple<double, VarianceLevel>>();

	public AngleHistory()
	{ 
		varianceTolerances.Add (new Tuple<double, VarianceLevel> (5, VarianceLevel.SuperHigh));
		varianceTolerances.Add (new Tuple<double, VarianceLevel> (3.5, VarianceLevel.High));
		varianceTolerances.Add (new Tuple<double, VarianceLevel> (1, VarianceLevel.Medium));
		varianceTolerances.Add (new Tuple<double, VarianceLevel> (0.3, VarianceLevel.Low));
		varianceTolerances.Add (new Tuple<double, VarianceLevel> (0, VarianceLevel.None));
	}

	public void Add(Tuple<int, float, InPiecePosition> observedAngle)
	{
		var lastAngle = angles.LastOrDefault ();
		IsIncreasing = lastAngle == null
			|| (observedAngle.Item1 == 1 + lastAngle.Item1
			&& observedAngle.Item2 >= lastAngle.Item2);

		angles.Add (observedAngle);

		double variance = (angles.Count > 1) 
			? angles [angles.Count - 1].Item2 - angles [angles.Count - 2].Item2 
			: 0;

		try
		{
			VarianceLevel = IsIncreasing 
				? varianceTolerances.First (t => variance >= t.Item1).Item2
				: VarianceLevel.None;
		}
		catch(Exception) {
			VarianceLevel = VarianceLevel.None;
		}

	}

	public void Reset ()
	{
		angles.Clear ();
		VarianceLevel = VarianceLevel.None;
	}

	public VarianceLevel VarianceLevel {
		get;
		private set;
	}

	public bool IsIncreasing {
		get;
		private set;
	}


	public InPiecePosition FirstPiecePosition {
		get {
			return angles.Count != 0
				? angles.First ().Item3
					: null;
		}

	}

	public float LastAngle
	{
		get {
			return angles.Count != 0
				? angles.Last ().Item2
					: 0;
		}
	}

		
	public override string ToString ()
	{
		StringBuilder result = new StringBuilder ();
		foreach (var t in angles) {
			result.AppendLine ("GameTick: " + t.Item1 + "\tAngle: " + t.Item2);
		}
		return result.ToString ();
	}
}






