using System;
using System.Collections.Generic;
using System.Linq;

public class SpeedData : ICloneable
{
	public double MaxSafeSpeed;
	public double PendingSpeed;
	public int? PromotionPosition;
	public readonly int Position;

	public SpeedData (int position)
	{
		this.Position = position;
	}

	public object Clone ()
	{
		return MemberwiseClone ();
	}
}

public class SpeedTelemetry : 
	IObserver<CarPositionsMsg>, 
	IObserver<CrashMsg>, 
	IObserver<LapFinishedMsg>
{
	private Dictionary<int, int> pieceOffsets = new Dictionary<int, int> ();
	private Dictionary<CarData, PositionHistory> carHistories = new Dictionary<CarData, PositionHistory> ();
	private int trackLength;
	private Track track;

	public SpeedTelemetry (RaceData raceData)
	{
		pieceOffsets.Add (0, 0); 
		for (int i = 1; i < raceData.track.pieces.Count; ++i) {
			var previousPiece = raceData.track.pieces [i - 1];
			if (previousPiece.IsStraight) {
				pieceOffsets.Add (i, 
					pieceOffsets [i - 1] + (int)previousPiece.length);
			} else {
				pieceOffsets.Add (i,
					pieceOffsets [i - 1] + (int)raceData.track.lanes.Max (l => previousPiece.Length (l)));
			}
		}
		trackLength = pieceOffsets [raceData.track.pieces.Count - 1]
		+ (int)raceData.track.lanes.Max (l => raceData.track.pieces.Last ().Length (l));
		track = raceData.track;

		InitializeCarHistories (raceData);

	}

	void InitializeCarHistories (RaceData raceData)
	{
		int capacity = (int)raceData.track.lanes.Max (l => l.length) / 5 + 1;
		List<SpeedData> speedData = new List<SpeedData> (capacity);
		for (int i = 0; i < capacity; ++i) {
			speedData.Add (new SpeedData (i * 5));
		}

		foreach (var car in raceData.cars) {
			List<SpeedData> clonedData = speedData.Select<SpeedData, SpeedData> (sd => (SpeedData)sd.Clone ()).ToList ();
			var singleHistory = new PositionHistory (clonedData, pieceOffsets, trackLength);
			carHistories.Add (car.id, singleHistory);
		}
	}

	public void StartGame()
	{
		foreach (var history in carHistories.Values) {
			history.ResetCurrentPosition ();
		}
	}

	/// <summary>
	/// Returns the max observed speed (across all cars, laps and lanes) at a position
	/// Speed expressed in distance-unit per gameTick
	/// </summary>
	/// <returns>The max observed speed.</returns>
	/// <param name="position">Position.</param>
	public double GetMaxObservedSpeed (InPiecePosition position)
	{
		return carHistories.Values.Max (ch => ch.GetMaxObservedSpeed (position));
	}

	/// <summary>
	/// Returns the max observed speed for a given car (across all laps and lanes) at a position
	/// Speed expressed in distance-unit per gameTick
	/// </summary>
	/// <returns>The max observed speed.</returns>
	/// <param name="position">Position.</param>
	public double GetMaxObservedSpeed (CarData carId, InPiecePosition position)
	{
		return carHistories [carId].GetMaxObservedSpeed (position);
	}

	public String GetFormattedTelemetry ()
	{
		String telemetry = String.Empty;
		foreach (var key in carHistories.Keys) {
			telemetry += String.Format ("Car {0} ({1}): {2}\n\n", 
				key.name, key.color, carHistories [key].ToString ());
		}
		return telemetry;
	}

	/// <summary>
	/// Returns the max observed speeds across the whole track, 
	/// segmented into 5% sections of the track.
	/// e.g, { 3.0, 12.8, 3.4, ...} means that at some point at a distance of 6%-10% 
	/// from the start line, a car peaked at 12.8 units-per-gameTick; this did not need 
	/// to be sustained across the entire track section
	/// </summary>
	/// <returns>The max observed speeds summary.</returns>
	public List<double> GetMaxObservedSpeedsSummary()
	{
		List<List<double>> allCarMaxSpeeds = new List<List<double>> ();
		foreach (var key in carHistories.Keys) {
			allCarMaxSpeeds.Add (carHistories [key].MaxSpeeds ());
		}

		List<double> aggregateMaxSpeeds = new List<double> ();

		for (int i = 0; i < allCarMaxSpeeds [0].Count; i++) {
			aggregateMaxSpeeds.Add (allCarMaxSpeeds.Select (x => x [i]).Max ());
		}

		var groupSize = aggregateMaxSpeeds.Count () / 20;
		List<double> condensedSpeed = new List<double> ();
		for (int i = 0; i < aggregateMaxSpeeds.Count () / groupSize; ++i) {
			condensedSpeed.Add (aggregateMaxSpeeds.Skip (groupSize * i).Take (groupSize).Average ());
		}

		return condensedSpeed;
	}

	/// <summary>
	/// Similar to GetMaxObservedSpeedsSummary()
	/// This is equivalent 
	/// segmented into 5% sections of the track.
	/// e.g, { 3.0, 12.8, 3.4, ...} means that at some point at a distance of 6%-10% 
	/// from the start line, a car peaked at 12.8 units-per-gameTick; this did not need 
	/// to be sustained across the entire track section
	/// </summary>
	/// <returns>The max observed speeds summary.</returns>


	/// <summary>
	/// Similar to GetMaxObservedSpeedsSummary(). This is equivalent 
	/// to the MaxObservedSpeedsSummary for the specified car minus
	/// the results of GetMaxObserevedSpeedsSummary()
	/// </summary>
	/// <returns>The telemetry summary.</returns>
	/// <param name="id">Identifier.</param>
	public List<double> DifferentialTelemetrySummary (CarData id)
	{
		List<List<double>> allCarMaxSpeeds = new List<List<double>> ();
		foreach (var key in carHistories.Keys) {
			if (!id.Equals (key)) {
				allCarMaxSpeeds.Add (carHistories [key].MaxSpeeds ());
			}
		}

		var targetMaxSpeeds = carHistories [id].MaxSpeeds ();
		List<double> speedDiff = new List<double> (targetMaxSpeeds.Count);

		if (allCarMaxSpeeds.Count != 0) {
			List<double> aggregateMaxSpeeds = new List<double> ();

			for (int i = 0; i < allCarMaxSpeeds [0].Count; i++) {
				aggregateMaxSpeeds.Add (allCarMaxSpeeds.Select (x => x [i]).Max ());
			}
			for (int i = 0; i < targetMaxSpeeds.Count (); ++i) {
				speedDiff.Add (targetMaxSpeeds [i] - aggregateMaxSpeeds [i]);
			}
		} else {
			for (int i = 0; i < targetMaxSpeeds.Count (); ++i) {
				speedDiff.Add (0);
			}
		}

		var groupSize = targetMaxSpeeds.Count () / 20;
		List<double> condensedSpeedDiff = new List<double> ();
		for (int i = 0; i < targetMaxSpeeds.Count () / groupSize; ++i) {
			condensedSpeedDiff.Add (speedDiff.Skip (groupSize * i).Take (groupSize).Average ());
		}

		return condensedSpeedDiff;

	}

	public List<double> DifferentialTelemetry (CarData id, InPiecePosition startPosition, InPiecePosition endPosition, int segmentSize=10)
	{
		List<double> speedDiff = new List<double> ();

		InPiecePosition iteratorPosition = new InPiecePosition () { 
			pieceIndex = startPosition.pieceIndex,
			inPieceDistance = startPosition.inPieceDistance
		};

		int breakCount = 0;
		while (iteratorPosition.IsBefore (endPosition) && ++breakCount < 500) {
			speedDiff.Add (GetMaxObservedSpeed (id, iteratorPosition) - GetMaxObservedSpeed (iteratorPosition));

			iteratorPosition = track.TryGetAdvancedPiecePosition (iteratorPosition, 10);
			if (iteratorPosition == null)
				breakCount = 500;
		}

		if (breakCount == 500)
			speedDiff.Clear ();

		return speedDiff;
	}

	#region IObserver<CarPositionsMsg> implementation

	void IObserver<CarPositionsMsg>.OnCompleted ()
	{
	}

	void IObserver<CarPositionsMsg>.OnError (Exception error)
	{
	}

	void IObserver<CarPositionsMsg>.OnNext (CarPositionsMsg value)
	{
		foreach (var carPosition in value.positions) {
			PositionHistory positionHistory = null;
			if (carHistories.TryGetValue (carPosition.id, out positionHistory)) {
				positionHistory.OnNext (carPosition);
			} else {
				Console.WriteLine (String.Format ("****ERROR: received carposition message for car {0} ({1}) but no telemetry match found",
					carPosition.id.name, carPosition.id.color));
			}

		}
	}

	#endregion

	#region IObserver<CrashMsg> implementation

	void IObserver<CrashMsg>.OnCompleted ()
	{
	}

	void IObserver<CrashMsg>.OnError (Exception error)
	{
	}

	void IObserver<CrashMsg>.OnNext (CrashMsg value)
	{
		PositionHistory positionHistory = null;
		if (carHistories.TryGetValue (value.carId, out positionHistory)) {
			positionHistory.ClearPendingSpeeds ();
		} else {
			Console.WriteLine (String.Format ("****ERROR: received crash message for car {0} ({1}) but no telemetry match found",
				value.carId.name, value.carId.color));
		}
	}

	#endregion

	#region IObserver<LapFinishedMsg> implementation

	void IObserver<LapFinishedMsg>.OnCompleted ()
	{
	}

	void IObserver<LapFinishedMsg>.OnError (Exception error)
	{
	}

	void IObserver<LapFinishedMsg>.OnNext (LapFinishedMsg value)
	{
		PositionHistory positionHistory = null;
		if (carHistories.TryGetValue (value.LapFinishedData.car, out positionHistory)) {
			positionHistory.PromotePendingSpeeds (trackLength);
		} else {
			Console.WriteLine (String.Format ("****ERROR: received lap finished message for car {0} ({1}) but no telemetry match found",
				value.LapFinishedData.car.name, value.LapFinishedData.car.color));
		}
	}

	#endregion

}

public class PositionHistory : IObserver<CarPosition>
{
	private Dictionary<int, int> pieceOffsets = new Dictionary<int, int> ();
	private int trackLength;
	private List<SpeedData> speedData = new List<SpeedData> ();
	private float? priorPosition;
	public int PendingLength;

	public PositionHistory (List<SpeedData> speedData, Dictionary<int, int> pieceOffsets, int trackLength)
	{
		this.speedData = speedData;
		this.trackLength = trackLength;
		this.pieceOffsets = pieceOffsets;

		PendingLength = trackLength / 10;
	}

	public double GetMaxObservedSpeed (InPiecePosition position)
	{
		var distance = pieceOffsets [position.pieceIndex] + position.inPieceDistance;
		return speedData.Last (sd => sd.Position <= distance).MaxSafeSpeed;
	}

	#region IObserver implementation

	public void OnCompleted ()
	{
	}

	public void OnError (Exception error)
	{
	}

	public void OnNext (CarPosition value)
	{
		var position = value.piecePosition;

		float currPosition = (position.pieceIndex < pieceOffsets.Count () - 1) ?
		                     Math.Min (pieceOffsets [position.pieceIndex] + position.inPieceDistance, 
			                     pieceOffsets [position.pieceIndex + 1])
		                     : pieceOffsets [position.pieceIndex] + position.inPieceDistance;
	
		if (priorPosition != null) {
			int promotionPosition = ((int)(currPosition + PendingLength)) % trackLength;
			if (priorPosition <= currPosition) {
				float currSpeed = currPosition - (float)priorPosition;
				foreach (var sd in speedData.Where(s => priorPosition < s.Position && s.Position <= currPosition)) {
					sd.PendingSpeed = currSpeed;
					sd.PromotionPosition = promotionPosition;
				}
			} else {
				float currSpeed = currPosition + trackLength - (float)priorPosition;
				foreach (var sd in speedData.Where (s => priorPosition < s.Position || s.Position <= currPosition)) {
					sd.PendingSpeed = currSpeed;
					sd.PromotionPosition = promotionPosition;
				}
			}
		}
		PromotePendingSpeeds (currPosition);
		priorPosition = currPosition;

	}

	#endregion

	public void PromotePendingSpeeds (float currPosition)
	{
		speedData.ForEach (sd => {
			if (sd.PromotionPosition != null 
				&& (sd.PromotionPosition <= currPosition
					|| (sd.PromotionPosition >= trackLength - PendingLength
						&& currPosition <= PendingLength)
				)) {
				sd.MaxSafeSpeed = Math.Max (sd.MaxSafeSpeed, sd.PendingSpeed);
				sd.PendingSpeed = 0;
				sd.PromotionPosition = null;
			}
		}
		);
	}

	public void ResetCurrentPosition()
	{
		ClearPendingSpeeds ();
		priorPosition = null;
	}

	public void ClearPendingSpeeds ()
	{
		speedData.ForEach (sd => {
			if (sd.PromotionPosition != null) {
				sd.PendingSpeed = 0;
				sd.PromotionPosition = null;
			}
		});
	}

	public List<double> MaxSpeeds ()
	{
		return speedData.Select (s => s.MaxSafeSpeed).ToList ();
	}

	public override string ToString ()
	{
		return string.Join ("; ", speedData.Select (s => s.MaxSafeSpeed.ToString ("n2")));
	}
}

