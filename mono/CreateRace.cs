﻿using System;
using Newtonsoft.Json;

class CarBot{
	public string name { get; set;}
	public string key {get; set;}
}
class CreateRaceData{
	public CarBot botId { get; set; }
	public string trackName { get; set; }
	public string password { get; set; }
	public int carCount { get; set; }
}
