using System;
using System.Linq;

public class CrashMonitor : IObserver<CrashMsg>, IObserver<CarPositionsMsg>
{
	CarData car;
	RaceData race;
	double trackLength;

	private AngleHistory angleHistory = new AngleHistory();

	public CrashMonitor(RaceData raceData, CarData mycar)
	{
		car = mycar;
		race = raceData;
		trackLength =  race.track.lanes.Average (l => l.length);
	}


	#region CrashMonitoring

	void IObserver<CrashMsg>.OnCompleted ()
	{
	}

	void IObserver<CrashMsg>.OnError (Exception error)
	{
	}

	void IObserver<CrashMsg>.OnNext (CrashMsg value)
	{
		Console.WriteLine ("Got a Crash for "+ value.carId.color);
		if (value.carId.color == car.color) {
			/*
			 * Console.WriteLine ("Got a Crash for me");
			Console.WriteLine ("Variance Level is " + angleHistory.VarianceLevel.ToString());
			Console.WriteLine ("Last angle is " + angleHistory.LastAngle.ToString ());
			Console.WriteLine (angleHistory.ToString ());
			*/


			int currentPiece = race.track.currentPosition.piecePosition.pieceIndex;
			int previousPiece = race.track.PreviousPiece (currentPiece);
			int twoPiecesBefore = race.track.PreviousPiece (previousPiece);
			int threePiecesBefore = race.track.PreviousPiece (twoPiecesBefore);
			UnverifyAngle (previousPiece, false);
			UnverifyAngle (twoPiecesBefore, false);
			UnverifyAngle (threePiecesBefore, true);

			UpdateThrottle (race.track.currentPosition.piecePosition, angleHistory.VarianceLevel);


		}
	}
		

	private double GetThrottleMultiplierForVariance(VarianceLevel varianceLevel)
	{
		switch (varianceLevel) {
		case VarianceLevel.Low:
		case VarianceLevel.None:
		default:
			return 0.7;
		case VarianceLevel.Medium:
			return 0.65;
		case VarianceLevel.SuperHigh:
		case VarianceLevel.High:
			return 0.55;
		}
	}

	private double GetAdjustmentDistanceForVariance(VarianceLevel varianceLevel)
	{
		switch (varianceLevel) {
		case VarianceLevel.Low:
		case VarianceLevel.None:
		default:
			return 0.05 * trackLength;
		case VarianceLevel.Medium:
			return 0.075 * trackLength;
		case VarianceLevel.SuperHigh:
		case VarianceLevel.High:
			return 0.11 * trackLength;
		}
	}


	void UpdateThrottle(InPiecePosition crashPosition, VarianceLevel varianceLevel)
	{
		double throttleMultiplier = GetThrottleMultiplierForVariance (varianceLevel);
		double adjustmentDistance = GetAdjustmentDistanceForVariance (varianceLevel);
		Console.WriteLine ("update track for distance= " + adjustmentDistance + "( " + trackLength + " )");

		InPiecePosition adjustingPosition = race.track.TryGetPriorPiecePosition (crashPosition, (int) adjustmentDistance);
		while (adjustingPosition.pieceIndex != crashPosition.pieceIndex) {
			var piecePlan = race.track.pieces [adjustingPosition.pieceIndex].plan;
			foreach (var inst in piecePlan) {
				if (inst.start >= adjustingPosition.inPieceDistance) {
					var msg = inst.instructions.First ();
					if (msg is  Throttle) {
						((Throttle)msg).value = ((Throttle)msg).value * throttleMultiplier;
					}
				}
			}
			Console.WriteLine("Piece " + adjustingPosition.pieceIndex + " " + race.track.pieces [adjustingPosition.pieceIndex].PrintPiece (true));
			adjustingPosition.inPieceDistance = 0;
			adjustingPosition.pieceIndex = race.track.NextPiece (adjustingPosition.pieceIndex);
		}

		foreach (var inst in race.track.pieces [crashPosition.pieceIndex].plan) {
			if (inst.start <= crashPosition.inPieceDistance) {
				var msg = inst.instructions.First ();
				if (msg is  Throttle) {
					((Throttle)msg).value = ((Throttle)msg).value * throttleMultiplier;
				}
			}
		}
		Console.WriteLine("Piece " + adjustingPosition.pieceIndex + " " +race.track.pieces [crashPosition.pieceIndex].PrintPiece (true));

	}

	void UpdateThrottle(int idx, double multiplier)
	{
		Console.WriteLine ("Update instructions for piece: " + idx);
		var piecePlan = race.track.pieces [idx].plan;
		foreach (var inst in piecePlan) {
			var msg = inst.instructions.First ();
			if (msg is  Throttle) {
				((Throttle)msg).value = ((Throttle)msg).value * multiplier;
			}
		}
		Console.WriteLine(race.track.pieces [idx].PrintPiece (true));
	}


	void UnverifyAngle(int idx, bool onlyIfAngled)
	{
		var piecePlan = race.track.pieces [idx].plan;
		foreach (var inst in piecePlan) {
			if (onlyIfAngled) {
				inst.isSlipAngleVerified = race.track.pieces [idx].IsStraight || false;
			} else {
				inst.isSlipAngleVerified = false;
			}
		}
	}
	#endregion


	#region Position monitoring
	void IObserver<CarPositionsMsg>.OnCompleted ()
	{
	}

	void IObserver<CarPositionsMsg>.OnError (Exception error)
	{
	}

	void IObserver<CarPositionsMsg>.OnNext (CarPositionsMsg value)
	{
		var myPosition = value.positions.First (c => c.id.Equals(car));
		if (Math.Abs (myPosition.angle) > 20) {
			angleHistory.Add (new Tuple<int, float, InPiecePosition> (value.gameTick, Math.Abs (myPosition.angle), myPosition.piecePosition));
		} else {
			var firstPiecePosition = angleHistory.FirstPiecePosition;
			if (firstPiecePosition != null) {
				VerifyAngles (firstPiecePosition, myPosition.piecePosition);
			} else {
				VerifyAngles (new InPiecePosition () 
					{ inPieceDistance = 0, 
						pieceIndex = race.track.PreviousPiece(myPosition.piecePosition.pieceIndex) 
					}, 
					myPosition.piecePosition);
			}
			angleHistory.Reset ();
		}

		if (angleHistory.LastAngle > 32 && angleHistory.VarianceLevel >= VarianceLevel.Medium) {
			AdjustUnverifiedPlans (myPosition);
		} else {
			CarPositionMonitor.eBrake = 0;
		}

		if (value.gameTick % 500 == 0) {
			int unverifiedPieces = race.track.pieces.Count (p => p.plan.Any (a => !a.isSlipAngleVerified));
			Console.WriteLine ("unverified pieces = " + unverifiedPieces);
		}
	}


	void VerifyAngles (InPiecePosition firstPiecePosition, InPiecePosition currentPosition)
	{
		int currentIndex = firstPiecePosition.pieceIndex;
		while (currentIndex != currentPosition.pieceIndex) {
			foreach (var p in race.track.pieces[currentIndex].plan) {
				p.isSlipAngleVerified = true;
			}
			currentIndex = race.track.NextPiece (currentIndex);
		}
		foreach (var p in race.track.pieces[currentIndex].plan.Where(s => s.start < currentPosition.inPieceDistance)) {
			p.isSlipAngleVerified = true;
		}
	}


	private void AdjustUnverifiedPlans (CarPosition myPosition)
	{
		int currentPieceIndex = myPosition.piecePosition.pieceIndex;
		var futurePlansInSamePiece = race.track.pieces[currentPieceIndex].plan.Where(p => p.start > myPosition.piecePosition.inPieceDistance);
		var plansInNextPiece = race.track.pieces [race.track.NextPiece (currentPieceIndex)].plan;
		if (futurePlansInSamePiece.Any(p => !p.isSlipAngleVerified) 
			|| plansInNextPiece.Any(p => !p.isSlipAngleVerified)) {
			CarPositionMonitor.eBrake = 10;
			return;
		}
	}




	#endregion
}
