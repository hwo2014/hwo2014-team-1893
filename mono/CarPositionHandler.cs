﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public class CarPositionHandler : IObservable<CarPosition>
{
	private List<IObserver<CarPosition>> observers;
	public CarPositionHandler ()
	{
		observers = new List<IObserver<CarPosition>> ();
	}

	public IDisposable Subscribe(IObserver<CarPosition> observer)
	{
		// Check whether observer is already registered. If not, add it 
		if (! observers.Contains(observer))
			observers.Add(observer);
		return new Unsubscriber<CarPosition>(observers, observer);
	}

	public void CarStatus(CarPosition car)
	{
		foreach (var observer in observers)
			observer.OnNext(car);
	}
}

internal class Unsubscriber<CarPosition> : IDisposable
{
	private List<IObserver<CarPosition>> _observers;
	private IObserver<CarPosition> _observer;

	internal Unsubscriber(List<IObserver<CarPosition>> observers, IObserver<CarPosition> observer)
	{
		this._observers = observers;
		this._observer = observer;
	}

	public void Dispose() 
	{
		if (_observers.Contains(_observer))
			_observers.Remove(_observer);
	}
}


public class CarPositionMonitor : IObserver<CarPosition>
{
	public static int eBrake = 0;

	private IDisposable cancellation;
	private RaceData race;
	private StreamWriter writer;
	public CarPositionMonitor(RaceData raceInfo, StreamWriter wrt)
	{
		this.race = raceInfo;
		this.writer = wrt;
	}

	public virtual void Subscribe(CarPositionHandler provider)
	{
		cancellation = provider.Subscribe(this);
	}
	public virtual void Unsubscribe()
	{
		cancellation.Dispose();
	}

	public virtual void OnCompleted() 
	{
	}

	// No implementation needed: Method is not called by the CarPositionHandler class. 
	public virtual void OnError(Exception e)
	{
		// No implementation.
	}

	bool dummyTurbo(CarPosition position, Track track, int totalLaps)
	{
		if (position.piecePosition.lap == totalLaps - 1) {
			for (int i = position.piecePosition.pieceIndex; i < track.pieces.Count; ++i) {
				if (!track.pieces [i].IsStraight)
					return false;
			}
			return true;
		}

		return false;
	}

	// Update information. 
	public virtual void OnNext(CarPosition info)
	{
		if (eBrake > 0) {
			eBrake--;
			Console.WriteLine ("sending throttle 0.05");
			send(new Throttle(0.05));
		}


		race.track.currentPosition = info;

		InPieceStrategy st = race.track.GetPieceStrategy(info);


		SendMsg msg = st.instructions.FirstOrDefault ();
		if (msg is Throttle) {
			info.currentThrottle = ((Throttle)msg).value;
			if (race.track.IsTurboAvailable && dummyTurbo (info, race.track, race.raceSession.laps))
				msg = new Turbo (){ value = "Yayyyyyy" };
		}
		/*if(race.writeToLog)
			Console.WriteLine ("Send Msg: " + msg.ToJson());*/
		send (msg);
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
}