using System;

public class CarData
{
	public string name { get; set; }
	public string color { get; set; }

	public override bool Equals (object obj)
	{
		CarData carDataObj = obj as CarData;
		return carDataObj != null && name.Equals (carDataObj.name) && color.Equals (carDataObj.color);
	}

	public override int GetHashCode ()
	{
		return name.GetHashCode () ^ color.GetHashCode ();
	}
} 


